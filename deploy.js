#!/usr/bin/env node
require('dotenv').config({ silent: true })
const List = require('listr')
const execa = require('execa')
const lists = [
  {
    title: 'build',
    task: () => execa.stdout('npm', ['run', 'build'])
  },
  {
    title: `deploy to ${process.env.AWS_S3_BUCKET}`,
    task: () =>
      execa.stdout('aws', [
        's3',
        'sync',
        'dist/',
        `s3://${process.env.AWS_S3_BUCKET}`,
        '--profile',
        process.env.AWS_PROFILE
      ])
  }
]

const tasks = new List(lists)
tasks
  .run()
  .then(() => {
    console.log(
      `\n Your website: http://${process.env.AWS_S3_BUCKET}.s3-website-${process
        .env.AWS_DEFAULT_REGION}.amazonaws.com/`
    )
  })
  .catch(err => console.log(err))
