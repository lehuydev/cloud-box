import { api } from 'services'
import configFactory from 'config'
const config = configFactory()

const PAGE_SIZE = config.page_size

// Constants
export const types = {
  // Get customers
  GET_CUSTOMERS: 'GET_CUSTOMERS',
  GET_CUSTOMERS_SUCCESS: 'GET_CUSTOMERS_SUCCESS',
  GET_CUSTOMERS_FAIL: 'GET_CUSTOMERS_FAIL',
  // Get customers_STATISTICS
  GET_CUSTOMERS_STATISTICS: 'GET_CUSTOMERS_STATISTICS',
  GET_CUSTOMERS_STATISTICS_SUCCESS: 'GET_CUSTOMERS_STATISTICS_SUCCESS',
  GET_CUSTOMERS_STATISTICS_FAIL: 'GET_CUSTOMERS_STATISTICS_FAIL',
  // Add user
  ADD_CUSTOMER: 'ADD_CUSTOMER',
  ADD_CUSTOMER_SUCCESS: 'ADD_CUSTOMER_SUCCESS',
  ADD_CUSTOMER_FAIL: 'ADD_CUSTOMER_FAIL',
  // Edit CUSTOMER
  EDIT_CUSTOMER: 'EDIT_CUSTOMER',
  EDIT_CUSTOMER_SUCCESS: 'EDIT_CUSTOMER_SUCCESS',
  EDIT_CUSTOMER_FAIL: 'EDIT_CUSTOMER_FAIL',
  //Delete CUSTOMERs
  DELETE_CUSTOMER: 'DELETE_CUSTOMER',
  DELETE_CUSTOMER_SUCCESS: 'DELETE_CUSTOMER_SUCCESS',
  DELETE_CUSTOMER_FAIL: 'DELETE_CUSTOMER_FAIL',
  // Get CUSTOMER info
  GET_CUSTOMER_INFO: 'GET_CUSTOMER_INFO',
  GET_CUSTOMER_INFO_SUCCESS: 'GET_CUSTOMER_INFO_SUCCESS',
  GET_CUSTOMER_INFO_FAIL: 'GET_CUSTOMER_INFO_FAIL',
  //Export CUSTOMERS info
  EXPORT_CUSTOMERS: 'EXPORT_CUSTOMERS',
  EXPORT_CUSTOMERS_SUCCESS: 'EXPORT_CUSTOMERS_SUCCESS',
  EXPORT_CUSTOMERS_FAIL: 'EXPORT_CUSTOMERS_FAIL'
}

// Get users
const getCustomers = ({ name, page, limit }) => {
  console.log(limit)

  return dispatch => {
    dispatch({ type: types.GET_CUSTOMERS })
    api
      .getCustomers({ name, page, limit })
      .then(res => {
        if (res && res.data) {
          dispatch({
            type: types.GET_CUSTOMERS_SUCCESS,
            payload: { list: res.data.data.users, page: res.data.data.paging }
          })
        } else {
          dispatch({ type: types.GET_CUSTOMERS_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.GET_CUSTOMERS_FAIL }))
  }
}
const getCustomersStatistics = () => {
  return dispatch => {
    dispatch({ type: types.GET_CUSTOMERS_STATISTICS })
    api
      .getCustomersStatistics()
      .then(res => {
        console.log(res)
        if (res && res.data) {
          dispatch({
            type: types.GET_CUSTOMERS_STATISTICS_SUCCESS,
            payload: { date: res.data.data }
          })
        } else {
          dispatch({ type: types.GET_CUSTOMERS_STATISTICS_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.GET_CUSTOMERS_STATISTICS_FAIL }))
  }
}

// Add user
const addUser = newUser => {
  return dispatch => {
    dispatch({
      type: types.ADD_USER
    })
    api
      .addUser(newUser)
      .then(res => {
        if (res && res.status === 200) {
          dispatch({
            type: types.ADD_USER_SUCCESS
          })
          dispatch(
            actions.getUsers({
              name: '',
              page_index: 1,
              page_size: PAGE_SIZE
            })
          )
        } else {
          dispatch({
            type: types.ADD_USER_FAIL
          })
        }
      })
      .catch(err =>
        dispatch({
          type: types.ADD_USER_FAIL
        })
      )
  }
}
// Edit user
const editUser = (obj, id) => {
  return dispatch => {
    dispatch({ type: types.EDIT_USER })
    api
      .editUser(obj, id)
      .then(res => {
        if (res && res.data) {
          dispatch({ type: types.EDIT_USER_SUCCESS, payload: res.data })
          dispatch(
            actions.getUsers({
              name: '',
              page_index: 1,
              page_size: PAGE_SIZE
            })
          )
        } else {
          dispatch({ type: types.EDIT_USER_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.EDIT_USER_FAIL }))
  }
}
// Get user info
const getUserInfo = id => {
  return dispatch => {
    dispatch({ type: types.GET_USER_INFO })
    api
      .getUserInfo(id)
      .then(res => {
        if (res && res.data.retData) {
          dispatch({
            type: types.GET_USER_INFO_SUCCESS,
            payload: res.data.retData
          })
        } else {
          dispatch({ type: types.GET_USER_INFO_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.GET_USER_INFO_FAIL }))
  }
}
// Delete
const deleteUser = deleUser => {
  return dispatch => {
    dispatch({ type: types.DELETE_USER })
    api
      .deleteUser(deleUser)
      .then(res => {
        if (res && res.data.retMessage) {
          dispatch({ type: types.DELETE_USER_SUCCESS })
          if (res.data.retCode !== 1002) {
            dispatch(
              actions.getUsers({
                name: '',
                page_index: 1,
                page_size: PAGE_SIZE
              })
            )
          }
        } else {
          dispatch({ type: types.DELETE_USER_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.DELETE_USER_FAIL }))
  }
}
//Export
const exportCustomers = ({ from, to }) => {
  return dispatch => {
    dispatch({ type: types.EXPORT_CUSTOMERS })
    api
      .exportCustomers({ from, to })
      .then(response => {
        console.log(response.data)
        console.log('chay vo day')
        if (response && response.data) {
          dispatch({
            type: types.EXPORT_CUSTOMERS_SUCCESS,
            payload: response.data.data
          })
          console.log('data is ' + response.data.data.link)
          //tu dong download
          const url = response.data.data.link
          const link = document.createElement('a')
          link.href = 'http://' + url
          document.body.appendChild(link)
          link.click()
          return
        }
        dispatch({
          type: types.EXPORT_CUSTOMERS_FAIL
        })
      })
      .catch(err => {
        dispatch({ type: types.EXPORT_CUSTOMERS_FAIL })
      })
  }
}

// Actions
export const actions = {
  getCustomers,
  getCustomersStatistics,
  exportCustomers
  //   addUser,
  //   editUser,
  //   getUserInfo,
  //   deleteUser,
  //   getAvailableUsers
}

// Selectors
export const selectors = {
  getPage: state => state.customer.page,
  getTotal: state => state.customer.page.pageTotal,
  getCustomersList: state => state.customer.list || [],
  getStatistics: state => state.customer.date || {},
  getPageTotal: state => state.customer.page || null,
  getCustomerInfo: state => state.customer.info || {},
  getLoading: (state, action) => state.ui.customer[action].isLoading
  // exportCustomers: state => state.customer.days || {},
}

const defaultState = {
  date: {},
  list: [],
  page: null,
  info: {}
  // days: {}
}

const customerReducer = (state = defaultState, action) => {
  const nextState = Object.assign({}, state)
  switch (action.type) {
    case types.GET_CUSTOMERS_SUCCESS:
      return {
        ...nextState,
        list: action.payload.list,
        page: action.payload.page
      }
    case types.GET_CUSTOMERS_STATISTICS_SUCCESS:
      return {
        ...nextState,
        date: action.payload.date
      }
    case types.EXPORT_CUSTOMERS_SUCCESS:
      return {
        ...nextState,
        link: action.payload
      }
    // case types.ADD_USER_SUCCESS:
    //   return {
    //     ...nextState
    //   }
    // case types.GET_USER_INFO_SUCCESS:
    //   return {
    //     ...nextState,
    //     info: action.payload
    //   }
    // case types.EDIT_USER_SUCCESS:
    //   return {
    //     ...nextState
    //   }
    // case types.GET_AVAILABLE_USERS_SUCCESS:
    //   return {
    //     ...nextState,
    //     listAvai: action.payload.listAvai,
    //     page: action.payload.page
    //   }
    default:
      return state
  }
}

export default customerReducer
