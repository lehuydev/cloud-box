import { combineReducers } from 'redux'

import ui from '@/redux/modules/ui-reducer'
import auth from '@/redux/modules/auth-reducer'
import account from '@/redux/modules/account-reducer'
import user from '@/redux/modules/user-reducer'
import customer from '@/redux/modules/customer-reducer'
import staff from '@/redux/modules/staff-reducer'
import question from '@/redux/modules/question-reducer'
import campaign from '@/redux/modules/campaign-reducer'
import piksal from '@/redux/modules/piksal-reducer'
import faq from '@/redux/modules/faq-reducer'

export const rootReducer = combineReducers({
  ui,
  auth,
  account,
  user,
  customer,
  staff,
  question,
  campaign,
  piksal,
  faq
})
export default rootReducer
