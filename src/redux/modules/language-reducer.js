const SELECT_LANGUAGE = 'SELECT_LANGUAGE'

import localeData from '@/locales/data.json'
import { addLocaleData } from 'react-intl'
import en from 'react-intl/locale-data/en'
import vi from 'react-intl/locale-data/vi'
addLocaleData([...en, ...vi])
const store = require('store')

// fix yarn test
if (!store.get) {
  store.get = () => null
}

const language = store.get('locale')
  ? store.get('locale')
  : (navigator.languages && navigator.languages[0]) ||
    navigator.language ||
    navigator.userLanguage ||
    'en-US'

const languageWithoutRegionCode = language.toLowerCase().split(/[_-]+/)[0]
const messages =
  localeData[languageWithoutRegionCode] || localeData[language] || localeData.en

const defaultState = {
  locale: languageWithoutRegionCode,
  messages: messages
}

const languageReducer = (state = defaultState, action) => {
  const nextState = Object.assign({}, state)
  switch (action.type) {
    case SELECT_LANGUAGE:
      store.set('locale', action.data)
      return {
        locale: action.data,
        messages: localeData[action.data] || localeData.en
      }

    default:
      return nextState
  }
}

export const selectLanguage = locale => ({
  type: SELECT_LANGUAGE,
  data: locale
})

export default languageReducer
