import { api } from 'services'
import configFactory from 'config'
const config = configFactory()
import Notification from 'components/notification'

const PAGE_SIZE = config.page_size

// Constants
export const types = {
  // Get campaigns
  GET_CAMPAIGNS: 'GET_CAMPAIGNS',
  GET_CAMPAIGNS_SUCCESS: 'GET_CAMPAIGNS_SUCCESS',
  GET_CAMPAIGNS_FAIL: 'GET_CAMPAIGNS_FAIL',
  // Get my campaigns
  GET_MYCAMPAIGNS: 'GET_MYCAMPAIGNS',
  GET_MYCAMPAIGNS_SUCCESS: 'GET_MYCAMPAIGNS_SUCCESS',
  GET_MYCAMPAIGNS_FAIL: 'GET_MYCAMPAIGNS_FAIL',
  // Add user
  ADD_CAMPAIGN: 'ADD_CAMPAIGN',
  ADD_CAMPAIGN_SUCCESS: 'ADD_CAMPAIGN_SUCCESS',
  ADD_CAMPAIGN_FAIL: 'ADD_CAMPAIGN_FAIL',
  // Update CAMPAIGN
  UPDATE_CAMPAIGN: 'UPDATE_CAMPAIGN',
  UPDATE_CAMPAIGN_SUCCESS: 'UPDATE_CAMPAIGN_SUCCESS',
  UPDATE_CAMPAIGN_FAIL: 'UPDATE_CAMPAIGN_FAIL',
  //Delete CAMPAIGNs
  DELETE_CAMPAIGN: 'DELETE_CAMPAIGN',
  DELETE_CAMPAIGN_SUCCESS: 'DELETE_CAMPAIGN_SUCCESS',
  DELETE_CAMPAIGN_FAIL: 'DELETE_CAMPAIGN_FAIL',
  // Get CAMPAIGN info
  GET_CAMPAIGN_INFO: 'GET_CAMPAIGN_INFO',
  GET_CAMPAIGN_INFO_SUCCESS: 'GET_CAMPAIGN_INFO_SUCCESS',
  GET_CAMPAIGN_INFO_FAIL: 'GET_CAMPAIGN_INFO_FAIL',
  // Get CAMPAIGN info
  CHECK_CODE: 'CHECK_CODE',
  CHECK_CODE_SUCCESS: 'CHECK_CODE_SUCCESS',
  CHECK_CODE_FAIL: 'CHECK_CODE_FAIL',
  // Get CAMPAIGN info
  REDEEM_CODE: 'REDEEM_CODE',
  REDEEM_CODE_SUCCESS: 'REDEEM_CODE_SUCCESS',
  REDEEM_CODE_FAIL: 'REDEEM_CODE_FAIL',
  // Get campaign stat
  GET_CAMPAIGNS_STATISTICS: 'GET_CAMPAIGNS_STATISTICS',
  GET_CAMPAIGNS_STATISTICS_SUCCESS: 'GET_CAMPAIGNS_STATISTICS_SUCCESS',
  GET_CAMPAIGNS_STATISTICS_FAIL: 'GET_CAMPAIGNS_STATISTICS_FAIL',
  // Get campaigns detail stat
  GET_CAMPAIGN_DETAIL_STATISTICS: 'GET_CAMPAIGN_DETAIL_STATISTICS',
  GET_CAMPAIGN_DETAIL_STATISTICS_SUCCESS:
    'GET_CAMPAIGN_DETAIL_STATISTICS_SUCCESS',
  GET_CAMPAIGN_DETAIL_STATISTICS_FAIL: 'GET_CAMPAIGN_DETAIL_STATISTICS_FAIL'
}

// Get campaign
const getCampaigns = () => {
  return dispatch => {
    dispatch({ type: types.GET_CAMPAIGNS })
    api
      .getCampaigns()
      .then(res => {
        if (res && res.data) {
          console.log(res)
          dispatch({
            type: types.GET_CAMPAIGNS_SUCCESS,
            payload: {
              list: res.data.data
            }
          })
        } else {
          dispatch({ type: types.GET_CAMPAIGNS_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.GET_CAMPAIGNS_FAIL }))
  }
}
// Get campaign
const addCampaign = obj => {
  console.log(obj)

  return dispatch => {
    dispatch({ type: types.ADD_CAMPAIGN })
    api
      .addCampaign(obj)
      .then(res => {
        if (res && res.data) {
          console.log(res)
          dispatch({
            type: types.ADD_CAMPAIGN_SUCCESS
          })
          console.log('YESSSSS')
          Notification('success', 'SUCCESS', res.data.message)
        } else {
          dispatch({ type: types.ADD_CAMPAIGN_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.ADD_CAMPAIGN_FAIL }))
  }
}

// Get campaign
const getCampaignInfo = id => {
  return dispatch => {
    dispatch({ type: types.GET_CAMPAIGN_INFO })
    api
      .getCampaignInfo(id)
      .then(res => {
        if (res && res.data) {
          dispatch({
            type: types.GET_CAMPAIGN_INFO_SUCCESS,
            payload: res.data.data
          })
        } else {
          dispatch({ type: types.GET_CAMPAIGN_INFO_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.GET_CAMPAIGN_INFO_FAIL }))
  }
}

// Check code
const checkCode = code => {
  return dispatch => {
    dispatch({ type: types.CHECK_CODE })
    api
      .checkCode(code)
      .then(res => {
        console.log(res)
        if (res && res.data && res.data.status_code === 1000) {
          dispatch({
            type: types.CHECK_CODE_SUCCESS,
            payload: res.data.data
          })
        } else {
          dispatch({ type: types.CHECK_CODE_FAIL })
          Notification('error', 'ERROR', res.data.message)
        }
      })
      .catch(err => {
        console.log(err)
        dispatch({ type: types.CHECK_CODE_FAIL })
        Notification('error', 'ERROR', res.data.message)
      })
  }
}

// Redeem Code
const redeemCode = code => {
  return dispatch => {
    dispatch({ type: types.REDEEM_CODE })
    api
      .redeemCode(code)
      .then(res => {
        console.log(res)

        if (res && res.data && res.data.status_code === 1000) {
          dispatch({
            type: types.REDEEM_CODE_SUCCESS,
            payload: res.data.data
          })
          Notification('success', 'SUCCESS', res.data.message)
        } else {
          dispatch({ type: types.REDEEM_CODE_FAIL })
          Notification('error', 'error', res.data.message)
        }
      })
      .catch(err => dispatch({ type: types.REDEEM_CODE_FAIL }))
  }
}

const campaignsStat = () => {
  return dispatch => {
    dispatch({ type: types.GET_CAMPAIGNS_STATISTICS })
    api
      .campaignsStat()
      .then(res => {
        console.log(res)
        if (res && res.data) {
          dispatch({
            type: types.GET_CAMPAIGNS_STATISTICS_SUCCESS,
            payload: { stat: res.data.data }
          })
        } else {
          dispatch({ type: types.GET_CAMPAIGNS_STATISTICS_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.GET_CAMPAIGNS_STATISTICS_FAIL }))
  }
}

const campaignDetailStat = id => {
  return dispatch => {
    dispatch({ type: types.GET_CAMPAIGN_DETAIL_STATISTICS })
    api
      .campaignDetailStat(id)
      .then(res => {
        console.log(res)
        if (res && res.data) {
          dispatch({
            type: types.GET_CAMPAIGN_DETAIL_STATISTICS_SUCCESS,
            payload: { stat: res.data.data }
          })
        } else {
          dispatch({ type: types.GET_CAMPAIGN_DETAIL_STATISTICS_FAIL })
        }
      })
      .catch(err =>
        dispatch({ type: types.GET_CAMPAIGN_DETAIL_STATISTICS_FAIL })
      )
  }
}

// Actions
export const actions = {
  getCampaigns,
  addCampaign,
  getCampaignInfo,
  checkCode,
  redeemCode,
  campaignsStat,
  campaignDetailStat
}

// Selectors
export const selectors = {
  getPage: state => state.campaign.page,
  getTotal: state => state.campaign.page.pageTotal,
  getCampaignsList: state => state.campaign.list || [],
  getPageTotal: state => state.campaign.page || null,
  getCampaignInfo: state => state.campaign.info || {},
  checkCode: state => state.campaign.codeInfo || {},
  redeemCode: state => state.campaign.codeInfo2 || {},
  getLoading: (state, action) => state.ui.campaign[action].isLoading,
  getStatistics: state => state.campaign.stat || {}
}

const defaultState = {
  stat: {},
  list: [],
  page: null,
  info: {},
  codeInfo: {},
  codeInfo2: {}

  // days: {}
}

const campaignReducer = (state = defaultState, action) => {
  const nextState = Object.assign({}, state)
  switch (action.type) {
    case types.GET_CAMPAIGNS_SUCCESS:
      return {
        ...nextState,
        list: action.payload.list,
        page: action.payload.page
      }
    case types.GET_CAMPAIGN_INFO_SUCCESS:
      return {
        ...nextState,
        info: action.payload
      }
    case types.CHECK_CODE_SUCCESS:
      return {
        ...nextState,
        codeInfo: action.payload
      }
    case types.REDEEM_CODE_SUCCESS:
      return {
        ...nextState,
        codeInfo2: action.payload
      }
    case types.GET_CAMPAIGNS_STATISTICS_SUCCESS:
      return {
        ...nextState,
        stat: action.payload.stat
      }
    case types.GET_CAMPAIGN_DETAIL_STATISTICS_SUCCESS:
      return {
        ...nextState,
        stat: action.payload.stat
      }
    // case types.ADD_USER_SUCCESS:
    //   return {
    //     ...nextState
    //   }
    // case types.GET_USER_INFO_SUCCESS:
    //   return {
    //     ...nextState,
    //     info: action.payload
    //   }
    // case types.EDIT_USER_SUCCESS:
    //   return {
    //     ...nextState
    //   }
    // case types.GET_AVAILABLE_USERS_SUCCESS:
    //   return {
    //     ...nextState,
    //     listAvai: action.payload.listAvai,
    //     page: action.payload.page
    //   }
    default:
      return state
  }
}

export default campaignReducer
