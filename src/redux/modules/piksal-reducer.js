import { api } from 'services'
import configFactory from 'config'
const config = configFactory()

const PAGE_SIZE = config.page_size

// Constants
export const types = {
  // Get campaigns
  GET_CAMPAIGNS: 'GET_CAMPAIGNS',
  GET_CAMPAIGNS_SUCCESS: 'GET_CAMPAIGNS_SUCCESS',
  GET_CAMPAIGNS_FAIL: 'GET_CAMPAIGNS_FAIL',
  // Get my campaigns
  GET_MYCAMPAIGNS: 'GET_MYCAMPAIGNS',
  GET_MYCAMPAIGNS_SUCCESS: 'GET_MYCAMPAIGNS_SUCCESS',
  GET_MYCAMPAIGNS_FAIL: 'GET_MYCAMPAIGNS_FAIL',
  // Add user
  ADD_CAMPAIGN: 'ADD_CAMPAIGN',
  ADD_CAMPAIGN_SUCCESS: 'ADD_CAMPAIGN_SUCCESS',
  ADD_CAMPAIGN_FAIL: 'ADD_CAMPAIGN_FAIL',
  // Update CAMPAIGN
  UPDATE_CAMPAIGN: 'UPDATE_CAMPAIGN',
  UPDATE_CAMPAIGN_SUCCESS: 'UPDATE_CAMPAIGN_SUCCESS',
  UPDATE_CAMPAIGN_FAIL: 'UPDATE_CAMPAIGN_FAIL',
  //Delete CAMPAIGNs
  DELETE_CAMPAIGN: 'DELETE_CAMPAIGN',
  DELETE_CAMPAIGN_SUCCESS: 'DELETE_CAMPAIGN_SUCCESS',
  DELETE_CAMPAIGN_FAIL: 'DELETE_CAMPAIGN_FAIL',
  // Get CAMPAIGN info
  GET_CAMPAIGN_INFO: 'GET_CAMPAIGN_INFO',
  GET_CAMPAIGN_INFO_SUCCESS: 'GET_CAMPAIGN_INFO_SUCCESS',
  GET_CAMPAIGN_INFO_FAIL: 'GET_CAMPAIGN_INFO_FAIL',
  // Get CAMPAIGN info
  GET_SUPLLIERS: 'GET_SUPLLIERS',
  GET_SUPLLIERS_SUCCESS: 'GET_SUPLLIERS_SUCCESS',
  GET_SUPLLIERS_FAIL: 'GET_SUPLLIERS_FAIL'
}

// Get campaign
const getCampaigns = () => {
  return dispatch => {
    dispatch({ type: types.GET_CAMPAIGNS })
    api
      .getCampaigns()
      .then(res => {
        if (res && res.data) {
          console.log(res)
          dispatch({
            type: types.GET_CAMPAIGNS_SUCCESS,
            payload: {
              list: res.data
            }
          })
        } else {
          dispatch({ type: types.GET_CAMPAIGNS_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.GET_CAMPAIGNS_FAIL }))
  }
}
// Add campaign
const addCampaign = obj => {
  console.log(obj)
  return dispatch => {
    dispatch({ type: types.ADD_CAMPAIGN })
    api
      .addCampaignPiksal(obj)
      .then(res => {
        console.log(res.data.data.helpdesks)
        if (res && res.data) {
          console.log(res)
          dispatch({
            type: types.ADD_CAMPAIGN_SUCCESS
          })
        } else {
          dispatch({ type: types.ADD_CAMPAIGN_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.ADD_CAMPAIGN_FAIL }))
  }
}

// getSupplier
const getSupplierPiksal = obj => {
  console.log(obj)

  return dispatch => {
    dispatch({ type: types.GET_SUPLLIERS })
    api
      .getSupplierPiksal()
      .then(res => {
        if (res && res.data) {
          console.log(res)
          dispatch({
            type: types.GET_SUPLLIERS_SUCCESS,
            payload: {
              supplierList: res.data
            }
          })
        } else {
          dispatch({ type: types.GET_SUPLLIERS_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.GET_SUPLLIERS_FAIL }))
  }
}
// Actions
export const actions = {
  getCampaigns,
  addCampaign,
  getSupplierPiksal
}

// Selectors
export const selectors = {
  getPage: state => state.piksal.page,
  getTotal: state => state.piksal.page.pageTotal,
  getCampaignsList: state => state.piksal.list || [],
  getSupplierList: state => state.piksal.supplierList || [],
  getPageTotal: state => state.piksal.page || null,
  getCampaignInfo: state => state.piksal.info || {},
  getLoading: (state, action) => state.ui.piksal[action].isLoading
}

const defaultState = {
  date: {},
  list: [],
  supplierList: [],
  page: null,
  info: {}
  // days: {}
}

const piksalReducer = (state = defaultState, action) => {
  const nextState = Object.assign({}, state)
  switch (action.type) {
    case types.GET_CAMPAIGNS_SUCCESS:
      return {
        ...nextState,
        list: action.payload.list
      }
    case types.GET_SUPLLIERS_SUCCESS:
      return {
        ...nextState,
        supplierList: action.payload.supplierList
      }
    // case types.GET_USER_INFO_SUCCESS:
    //   return {
    //     ...nextState,
    //     info: action.payload
    //   }
    // case types.EDIT_USER_SUCCESS:
    //   return {
    //     ...nextState
    //   }
    // case types.GET_AVAILABLE_USERS_SUCCESS:
    //   return {
    //     ...nextState,
    //     listAvai: action.payload.listAvai,
    //     page: action.payload.page
    //   }
    default:
      return state
  }
}

export default piksalReducer
