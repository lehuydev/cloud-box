import { api } from 'services'
import configFactory from 'config'
const config = configFactory()

const PAGE_SIZE = config.page_size

// Constants
export const types = {
  // Get staffs
  GET_STAFFS: 'GET_STAFFS',
  GET_STAFFS_SUCCESS: 'GET_STAFFS_SUCCESS',
  GET_STAFFS_FAIL: 'GET_STAFFS_FAIL',
  // Add user
  ADD_STAFF: 'ADD_STAFF',
  ADD_STAFF_SUCCESS: 'ADD_STAFF_SUCCESS',
  ADD_STAFF_FAIL: 'ADD_STAFF_FAIL',
  // Edit STAFF
  EDIT_STAFF: 'EDIT_STAFF',
  EDIT_STAFF_SUCCESS: 'EDIT_STAFF_SUCCESS',
  EDIT_STAFF_FAIL: 'EDIT_STAFF_FAIL',
  //Delete STAFFs
  DELETE_STAFF: 'DELETE_STAFF',
  DELETE_STAFF_SUCCESS: 'DELETE_STAFF_SUCCESS',
  DELETE_STAFF_FAIL: 'DELETE_STAFF_FAIL',
  // Get STAFF info
  GET_STAFF_INFO: 'GET_STAFF_INFO',
  GET_STAFF_INFO_SUCCESS: 'GET_STAFF_INFO_SUCCESS',
  GET_STAFF_INFO_FAIL: 'GET_STAFF_INFO_FAIL',
  // Get roles
  GET_ROLES: 'GET_ROLES',
  GET_ROLES_SUCCESS: 'GET_ROLES_SUCCESS',
  GET_ROLES_FAIL: 'GET_ROLES_FAIL',
  // Get roles
  CREATE_ACCOUNT: 'CREATE_ACCOUNT',
  CREATE_ACCOUNT_SUCCESS: 'CREATE_ACCOUNT_SUCCESS',
  CREATE_ACCOUNT_FAIL: 'CREATE_ACCOUNT_FAIL'
}

// Get users
const getStaffs = ({ name, page, limit }) => {
  console.log(limit)

  return dispatch => {
    dispatch({ type: types.GET_STAFFS })
    api
      .getStaffs({ name, page, limit })
      .then(res => {
        if (res && res.data) {
          dispatch({
            type: types.GET_STAFFS_SUCCESS,
            payload: { list: res.data.data.users, page: res.data.data.paging }
          })
        } else {
          dispatch({ type: types.GET_STAFFS_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.GET_STAFFS_FAIL }))
  }
}

const getRoles = ({ name, page, limit }) => {
  return dispatch => {
    dispatch({ type: types.GET_ROLES })
    api
      .getRoles({ name, page, limit })
      .then(res => {
        if (res && res.data) {
          console.log(res.data.data)

          dispatch({
            type: types.GET_ROLES_SUCCESS,
            payload: res.data.data
          })
        } else {
          dispatch({ type: types.GET_ROLES_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.GET_ROLES_FAIL }))
  }
}

const addStaff = (obj, role_id) => {
  const email = obj.email
  return dispatch => {
    dispatch({ type: types.ADD_STAFF })
    api
      .addStaff(obj)
      .then(res => {
        if (res && res.data) {
          console.log(res)
          dispatch({
            type: types.ADD_STAFF_SUCCESS
          })
          const object = {
            user_id: res.data.data.id,
            email: email,
            role_id: role_id
          }
          dispatch(actions.createAccount(object))
        } else {
          dispatch({ type: types.ADD_STAFF_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.ADD_STAFF_FAIL }))
  }
}

const createAccount = obj => {
  return dispatch => {
    dispatch({ type: types.CREATE_ACCOUNT })
    api
      .createAccount(obj)
      .then(res => {
        if (res && res.data) {
          console.log('account' + res)
          dispatch({
            type: types.CREATE_ACCOUNT_SUCCESS
          })
        } else {
          dispatch({ type: types.CREATE_ACCOUNT_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.CREATE_ACCOUNT_FAIL }))
  }
}

// Actions
export const actions = {
  getStaffs,
  getRoles,
  addStaff,
  createAccount
}

// Selectors
export const selectors = {
  getPage: state => state.staff.page,
  getTotal: state => state.staff.page.pageTotal,
  getStaffsList: state => state.staff.list || [],
  getRolesList: state => state.staff.roleList || [],
  getPageTotal: state => state.staff.page || null,
  getStaffInfo: state => state.staff.info || {},
  getLoading: (state, action) => state.ui.staff[action].isLoading
  // exportStaffs: state => state.staff.days || {},
}

const defaultState = {
  date: {},
  list: [],
  roleList: [],
  page: null,
  info: {}
  // days: {}
}

const staffReducer = (state = defaultState, action) => {
  const nextState = Object.assign({}, state)
  switch (action.type) {
    case types.GET_STAFFS_SUCCESS:
      return {
        ...nextState,
        list: action.payload.list,
        page: action.payload.page
      }
    case types.GET_ROLES_SUCCESS:
      return {
        ...nextState,
        roleList: action.payload
      }
    // case types.ADD_USER_SUCCESS:
    //   return {
    //     ...nextState
    //   }
    // case types.GET_USER_INFO_SUCCESS:
    //   return {
    //     ...nextState,
    //     info: action.payload
    //   }
    // case types.EDIT_USER_SUCCESS:
    //   return {
    //     ...nextState
    //   }
    // case types.GET_AVAILABLE_USERS_SUCCESS:
    //   return {
    //     ...nextState,
    //     listAvai: action.payload.listAvai,
    //     page: action.payload.page
    //   }
    default:
      return state
  }
}

export default staffReducer
