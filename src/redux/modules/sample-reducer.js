import client from '@/client'
import { formatError } from '@/helper'
export const GET_SAMPLE = 'get_sample'
export const GET_SAMPLE_SUCCESS = 'get_sample_success'
export const GET_SAMPLE_ERROR = 'get_sample_error'

const defaultState = {}
const sampleReducer = (state = defaultState, action) => {
  const nextState = Object.assign({}, state)

  switch (action.type) {
    case GET_SAMPLE_SUCCESS:
      return Object.assign(nextState, action.data)

    default:
      return nextState
  }
}

export const getSampleApi = () => {
  return dispatch => {
    dispatch({
      type: GET_SAMPLE
    })

    client
      .api('/sample')
      .then(res =>
        dispatch({
          type: GET_SAMPLE_SUCCESS,
          data: res.data
        })
      )
      .catch(err =>
        dispatch({
          type: GET_SAMPLE_ERROR,
          reason: formatError(err)
        })
      )
  }
}

export default sampleReducer