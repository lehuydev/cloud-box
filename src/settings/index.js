export const themeConfig = {
  topbar: "themedefault",
  sidebar: "themedefault",
  layout: "themedefault",
  theme: "themedefault"
};

export const siteConfig = {
  siteName: "ReactJS",
  siteIcon: require("public/images/logo.svg")
};
