import configFactory from './config'
import axios from 'axios'
import * as qs from 'qs'

const config = configFactory(process.env.NODE_ENV)

class Client {
  constructor(opt = {}) {
    this.opt = Object.assign(this._getDefaultOption(), opt)
    this.request = axios.create({})
  }

  // @private
  _getDefaultOption() {
    return {
      timeout: 5000
    }
  }

  _initSocket(token) {
    if (this.sock) this.sock.close()
  }

  // @private
  _reset() {
    this.request = axios.create(this._getDefaultOption())
  }

  // @private
  _update(opt) {
    this.request = axios.create(Object.assign(this.opt, opt))
  }

  addTokenToHeader(token) {
    this._update({
      headers: {
        Authorization: 'Bearer ' + token
      }
    })

    this._initSocket(token)
  }

  removeToken() {
    if (this.opt.headers && this.opt.headers['Authorization']) {
      delete this.opt.headers['Authorization']
    }
    localStorage.setItem('token', null)
    this._update(this.opt)

    if (this.sock) this.sock.close()
  }

  login({ email, password }) {
    return this.api('/login', 'post', { email, password }).then(res => {
      this.addTokenToHeader(res.headers['Authorization'])
      return res
    })

    return this.request.post(endpoint.USER + '/login', { email, password })
  }

  logout() {
    localStorage.clear()
    return this.api('/logout', 'get').then(res => {
      this._reset()
      return res
    })
  }

  api(path, method = 'get', params = {}, headers) {
    return new Promise((resolve, reject) => {
      if (!path) {
        // path is required
        return reject(new Error('Used: api(path, method, params)'))
      }
      const token = localStorage.getItem('token')

      if (token) {
        this.addTokenToHeader(token)
      }
      // add '/' to path if missing

      // use v1 version

      if (path[0] !== '/') {
        path = '/' + path
      }

      path = config.api + path

      if (path.indexOf('?') !== -1) {
        path += '&_t=' + Date.now()
      } else {
        path += '?_t=' + Date.now()
      }

      // support shorthand api(path, params)
      // method will default to get
      if (arguments.length === 2 && String(method) == '[object Object]') {
        params = method
        method = 'get'
      }

      method = method.toLowerCase()
      if (['get', 'post', 'put', 'delete'].indexOf(method) === -1) {
        return reject(
          new Error(
            `ERR: ${method} not allowed, only allow get, post, put, delete`
          )
        )
      }
      console.log(path)
      this.request[method.toLowerCase()](path, params, headers)
        .then(resolve)
        .catch(reject)
    })
  }
}

export default new Client()
