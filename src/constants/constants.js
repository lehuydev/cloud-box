import React, { Component } from 'react'

export const hp = window.innerHeight
export const wp = window.innerWidth
import ic_QLND from 'public/images/ic_QLND.png'
import icHoiDap from 'public/images/ic_hoidap.png'
import icThongTin from 'public/images/ic_thongtin.png'
import icNext from 'public/images/ic_muiten.png'
import icThemMoi from 'public/images/ic_themmoi.png'
import icDropdown from 'public/images/ic_muiten2.png'
import icSearch from 'public/images/ic_timkiem.png'
import icCapNhat from 'public/images/ic_capnhat.png'
import icXoa from 'public/images/ic_xoa.png'
import icCaiDat from 'public/images/ic_caidat.png'
import icKHTT from 'public/images/ic_khthanthiet.png'
import icKhuyenMai from 'public/images/ic_khuyenmai.png'
import icNhanThongTin from 'public/images/ic_hoidap.png'
import icPhanTichDuLieu from 'public/images/ic_phantichdulieu.png'
import icPheuKH from 'public/images/ic_pheuKH.png'
import icQLNguoiDung from 'public/images/ic_QLND.png'
import icTongQuan from 'public/images/ic_tongquan.png'
import icTraLoiTuDong from 'public/images/ic_traloitudong.png'
import icFilter from 'public/images/ic_loc.png'
import icTaiFile from 'public/images/ic_taifile.png'
import icDangXuat from 'public/images/ic_dangxuat.png'
import icDoiMK from 'public/images/ic_doimatkhau.png'
import icAttachment from 'public/images/ic_file.png'
import icXemChiTiet from 'public/images/ic_xemchitiet.png'

import UTMHelve from 'public/fonts/UTMHelve.ttf'
import UTMHelve_Italic from 'public/fonts/UTMHelve_Italic.ttf'
import UTMHelveBold_Italic from 'public/fonts/UTMHelveBold_Italic.ttf'
import UTMHelveBold from 'public/fonts/UTMHelveBold.ttf'
import UTMHelvetIns from 'public/fonts/UTMHelvetIns.ttf'

export const Icons = {
  icQLNS: ic_QLND,
  icHoiDap: icHoiDap,
  icThongTin: icThongTin,
  icNext: icNext,
  icThemMoi: icThemMoi,
  icDropdown: icDropdown,
  icSearch: icSearch,
  icCapNhat: icCapNhat,
  icXoa: icXoa,
  //---bo sung them
  icCaiDat: icCaiDat,
  icKHTT: icKHTT,
  icKhuyenMai: icKhuyenMai,
  icNhanThongTin: icNhanThongTin,
  icPhanTichDuLieu: icPhanTichDuLieu,
  icPheuKH: icPheuKH,
  icQLNguoiDung: icQLNguoiDung,
  icTongQuan: icTongQuan,
  icTraLoiTuDong: icTraLoiTuDong,
  icFilter: icFilter,
  icTaiFile: icTaiFile,
  icAttachment: icAttachment,
  icDangXuat: icDangXuat,
  icDoiMK: icDoiMK,
  icXemChiTiet: icXemChiTiet
}

export const Colors = {
  blueMain: '#0099FF',
  grayBg: '#f9f9f9',
  grayStt: '#fafafa',
  grayBtn: '#ececec',
  whiteTxt: '#ffffff',
  blackTxt: '#000000',
  yellowStt: '#fffbbe',
  blueStt: '#b2d6fc',
  redStt: '#f89595',
  greenBtn: '#64bf1b',
  purpleBtn: '#A662DD'
}

export const Fonts = {
  normal: UTMHelve,
  italic: UTMHelve_Italic,
  boldItalic: UTMHelveBold_Italic,
  bold: UTMHelveBold,
  ins: UTMHelvetIns
}
// export const getWidth = () => {
//     this.props.windowWidth
// };
// export const getHeight = this.props.windowHeight;
