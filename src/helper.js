export const formatError = err => {
  console.log(err)

  if (err.response && err.response.data && err.response.data.reason) {
    return err.response.data.reason
  }

  return 'MSG_BAD_REQUEST'
}

export const getBase64 = file => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()

    reader.readAsDataURL(file)
    reader.onload = () => {
      console.log('get base 64 success')
      resolve(reader.result)
    }
    reader.onerror = error => {
      console.log('get base 64 error', error)
      reject(error)
    }
  })
}
export const queryPage = history => {
  return {
    set: (page: number, url: string) => {
      return history.push(url + `?page=${page}`)
    },
    get: () => {
      const query_string = history.location.search.substr(1)
      const query_page = Number(query_string.split('page=')[1])

      return query_page
    }
  }
}

export const ReducerFactory = ({
  defaultState,
  onStart,
  onSuccess,
  onError,
  onDefault
}) => {
  return (state = defaultState, action) => {
    const nextState = Object.assign({}, state)
    switch (action.type) {
      case onStart:
        return Object.assign(nextState, {
          isInit: false,
          isLoading: true
        })
      case onSuccess:
        return Object.assign(nextState, {
          isLoading: false,
          isSuccess: true,
          isError: false,
          reason: action.reason
        })
      case onError:
        return Object.assign(nextState, {
          isLoading: false,
          isError: true,
          isSuccess: false,
          reason: action.reason
        })
      case onDefault:
      default:
        return nextState
    }
  }
}
