const config = {
  development: {
    api: 'http://cb.studynav.com:8000/api',
    page_size: 20
  },
  production: {
    api: 'http://cb.studynav.com:8000/api',
    page_size: 20
  }
}
module.exports = envFlag => {
  return config[envFlag || 'development']
}
