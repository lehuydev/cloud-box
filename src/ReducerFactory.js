export const ReducerFactory = ({
  defaultState,
  onStart,
  onSuccess,
  onError,
  onDefault
}) => {
  return (state = defaultState, action) => {
    const nextState = Object.assign({}, state)
    switch (action.type) {
      case onStart:
        return Object.assign(nextState, {
          isInit: false,
          isLoading: true,
          isSuccess: false,
          isError: false
        })
      case onSuccess:
        return Object.assign(nextState, {
          isLoading: false,
          isSuccess: true,
          isError: false,
          reason: action.reason
        })
      case onError:
        return Object.assign(nextState, {
          isLoading: false,
          isError: true,
          isSuccess: false,
          reason: action.reason
        })
      case onDefault:
      default:
        return nextState
    }
  }
}
