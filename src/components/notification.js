import notification from './Feedback/notification'

const createNotification = (type, message, description) => {
  notification[type]({
    message,
    description,
    duration: 4
  })
}
export default createNotification
