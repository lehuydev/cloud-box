import React from 'react'
import { Link } from 'react-router-dom'

import PropTypes from 'prop-types'

import Popconfirm from 'components/Feedback/popconfirm'

export default class extends React.Component {
  static propTypes = {
    buttonLink: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.shape({
        href: PropTypes.string,
        value: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
      })
    ])
  }

  static defaultProps = {
    title: 'Title',
    okText: 'Ok',
    cancelText: 'cancel',
    buttonLabel: 'Button',
    onButtonCell: () => console.log('Button cell clicked!'),
    buttonLink: false,
    hasConfirm: true
  }

  render() {
    const {
      title,
      okText,
      cancelText,
      buttonLabel,
      hasConfirm,
      buttonLink,
      onCancel,
      onButtonCell
    } = this.props

    return buttonLink
      ? <Link to={buttonLink.href}>
          {buttonLink.value}
        </Link>
      : hasConfirm
        ? <Popconfirm
            title={title}
            okText={okText}
            cancelText={cancelText}
            onCancel={() => onCancel()}
            onConfirm={() => onButtonCell()}
          >
            <a>
              {buttonLabel}
            </a>
          </Popconfirm>
        : <a onClick={onButtonCell}>
            {buttonLabel}
          </a>
  }
}
