import React from 'react'
import { ComponentBreadCrumbWrapper } from './breadCrumbs.style'
import { Route, Link } from 'react-router-dom'
import { Breadcrumb, BreadcrumbItem } from 'reactstrap'

const routes = {
  '/thong-tin-thiet-bi': 'Thông tin thiết bị',
  '/thong-tin-thiet-bi/danh-sach-thiet-bi': 'Danh sách thiết bị',
  '/thong-tin-thiet-bi/danh-sach-thiet-bi/them-moi': 'Thêm mới',
  '/thong-tin-thiet-bi/danh-sach-thiet-bi/chinh-sua': 'Chỉnh sửa',

  '/nha-cung-cap': 'Nhà cung cấp',
  '/nha-cung-cap/them-moi': 'Thêm mới',
  '/nha-cung-cap/chinh-sua': 'Chỉnh sửa'
}

const findRouteName = url => routes[url]

const getPaths = pathname => {
  const paths = ['/']

  if (pathname === '/') return paths

  pathname.split('/').reduce((prev, curr, index) => {
    const currPath = `${prev}/${curr}`
    paths.push(currPath)
    return currPath
  })

  return paths
}

// const BreadcrumbsItem = ({...rest, match }) => {
//   const routeName = findRouteName(match.url)
//   if (routeName) {
//     return match.isExact
//       ? <BreadcrumbItem active>
//         {routeName}
//       </BreadcrumbItem>
//       : <BreadcrumbItem>
//         <Link to={match.url || ''}>
//           {routeName}
//         </Link>
//       </BreadcrumbItem>
//   }
//   return null
// }
const BreadcrumbsItem = ({ rest, match }) => {
  const routeName = findRouteName(match.url)
  if (routeName) {
    return match.isExact ? (
      <BreadcrumbItem active>{routeName}</BreadcrumbItem>
    ) : (
      <BreadcrumbItem>
        <Link to={match.url || ''}>{routeName}</Link>
      </BreadcrumbItem>
    )
  }
  return null
}

// const Breadcrumbs = ({ ...rest, location: { pathname }, match }) => {
//   const paths = getPaths(pathname)
//   return (
//     <Breadcrumb>
//       {paths.map(p => <Route key={'-' + p} path={p} component={BreadcrumbsItem} />)}
//     </Breadcrumb>
//   )
// }

const Breadcrumbs = ({ rest, location: { pathname }, match }) => {
  const paths = getPaths(pathname)
  return (
    <Breadcrumb>
      {paths.map(p => (
        <Route key={'-' + p} path={p} component={BreadcrumbsItem} />
      ))}
    </Breadcrumb>
  )
}

export default props => (
  <ComponentBreadCrumbWrapper className="isoComponentBreadCrumb w-50 ml-auto ">
    <Route path="/:path" component={Breadcrumbs} {...props} />
  </ComponentBreadCrumbWrapper>
)
