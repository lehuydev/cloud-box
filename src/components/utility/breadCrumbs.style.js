import styled from "styled-components";
import WithDirection from "settings/withDirection";

const WDComponentBreadCrumbWrapper = styled.div`
  nav {
    .breadcrumb {
      background-color: transparent;
      display: flex;
      justify-content: flex-end;
      padding-right: 0;
      letter-spacing: 0.3px;
      .breadcrumb-item {
        color: #2d3033;
        a {
          color: #01b5de;
        }
      }
      .breadcrumb-item + .breadcrumb-item::before {
        content: ">";
      }
    }
  }
`;

const ComponentBreadCrumbWrapper = WithDirection(WDComponentBreadCrumbWrapper);
export { ComponentBreadCrumbWrapper };
