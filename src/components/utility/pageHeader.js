import React from 'react'
import { ComponentTitleWrapper, ComponentTitleDiv } from './pageHeader.style'
import { Icons } from 'constants/constants'

export default props => (
  <ComponentTitleDiv>
    <img
      src={
        props.children == 'NGƯỜI DÙNG' || props.children == 'NHÂN VIÊN'
          ? Icons.icQLNS
          : props.children == 'FAQs' ||
            props.children == 'TƯ VẤN' ||
            props.children == 'DANH SÁCH CÂU HỎI'
          ? Icons.icHoiDap
          : props.children == 'ĐẶT HẸN' || props.children == 'NHẬN THÔNG TIN'
          ? Icons.icThongTin
          : props.children == 'NẠP MÃ KHUYẾN MÃI' ||
            props.children == 'DANH SÁCH CHIẾN DỊCH' ||
            props.children == 'TẠO CHIẾN DỊCH' ||
            props.children == 'HƯỚNG DẪN' ||
            props.children == 'CHI TIẾT CHIẾN DỊCH' ||
            props.children == 'DANH SÁCH CHIẾN DỊCH PIKSAL' ||
            props.children == 'TẠO CHIẾN DỊCH PIKSAL'
          ? Icons.icKhuyenMai
          : props.children == 'DANH SÁCH KHÁCH HÀNG'
          ? Icons.icKHTT
          : null
      }
      style={{ height: '25px' }}
    />
    <ComponentTitleWrapper className="isoComponentTitle">
      {props.children}
    </ComponentTitleWrapper>
  </ComponentTitleDiv>
)
