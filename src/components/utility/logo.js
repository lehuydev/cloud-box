import React from "react";
import { Link } from "react-router-dom";
import { siteConfig } from "settings";

export default ({ collapsed, image, small_image }) => {
  return (
    <div className="isoLogoWrapper">
      {collapsed ? (
        <div>
          <h3>
            <Link to="/">
              <img src={small_image} style={{ width: "40%", height: "40%" }} />
            </Link>
          </h3>
        </div>
      ) : (
        <h3>
          <Link to="/">
            <img src={image} />
          </Link>
        </h3>
      )}
    </div>
  );
};
