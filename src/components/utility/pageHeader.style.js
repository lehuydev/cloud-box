import styled from 'styled-components'
import { palette } from 'styled-theme'
import WithDirection from 'settings/withDirection'
import { Fonts } from 'constants/constants'

const WDComponentTitleWrapper = styled.h1`

  color: #484d5f;
  font-family:${Fonts.bold}
  font-size: 20px;
  font-weight: bold;
  padding-left: 14px;
`

const WDComponentTitleDiv = styled.div`
  display: flex ;
  margin : 51px 0 0 23px ;
`

const ComponentTitleWrapper = WithDirection(WDComponentTitleWrapper)
const ComponentTitleDiv = WithDirection(WDComponentTitleDiv)

export { ComponentTitleWrapper , ComponentTitleDiv }
