import React from 'react'
import { ImageIconsStyle , PStyle ,LeftBtn,RightBtn, ComponentBtn} from './styles/btn.style.js'
export default props =>

<ComponentBtn 
    style={{float:props.float}}
    onClick={props.onClick}
    type={props.type}
    autoFocus
    >
    <LeftBtn style={{backgroundColor: props.bgColor}}>
        <ImageIconsStyle src={props.icon} />
    </LeftBtn>
    <RightBtn>
        <PStyle>{props.title}</PStyle>
    </RightBtn>
</ComponentBtn>