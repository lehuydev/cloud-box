import styled from 'styled-components'
import { Colors } from 'constants/constants'

export const ImageIconsStyle = styled.img`
  height: 18px;
`
export const PStyle = styled.p`
  border: 0px;
  padding: 10px 15px;
  font-size: 14px;
  font-weight: bold;
`
export const ComponentBtn = styled.button`
  padding: 0;
  margin: 0 10px;
  background-color: inherit;
  display: flex;
  height: 40px;
  border: 0;
  transition: box-shadow 0.5s;
  border-radius: 8px !important;

  :focus {
    outline: 0;
  }
  :active {
    outline: none;
    border: none;
    color: ${Colors.blueMain};
    box-shadow: 2px 2px 2px grey;
  }
  :hover {
    background-color: inherit;
    font-style: 'bold';
    box-shadow: 2px 2px 2px grey;
  }
`

export const LeftBtn = styled.div`
    background-color : ${Colors.blueMain}
    height:40px;
    width:40px;
    line-height:40px
    border-radius: 8px 0 0 8px;
`

export const RightBtn = styled.div`
    background-color : ${Colors.grayBtn}
    height:40px
    border-radius: 0 8px 8px 0;
`
