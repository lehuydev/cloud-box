import React from "react"
import PropTypes from "prop-types"
import { Icon, Upload, Button as UploadButton } from "antd"

import { getBase64 } from "helper"

export default class extends React.Component {
  state = { fileList: [] }

  static propTypes = {
    title: PropTypes.string,
    maxLength: PropTypes.number,
    onUpload: PropTypes.func
  }

  static defaultProps = {
    title: "Browse...",
    maxLength: 1,
    onUpload: () => {}
  }

  onChange = ({ fileList }) => {
    this.setState({ fileList })
  }

  beforeUpload = async (file, fileList) => {
    const base64 = await getBase64(file)

    if (base64) {
      this.props.onUpload(base64)
    }
  }

  render() {
    const { fileList } = this.state
    const { title, maxLength, ...props } = this.props

    return (
      <Upload
        className="upload-list-inline"
        accept="image/*"
        listType="picture"
        fileList={fileList}
        onChange={this.onChange}
        beforeUpload={this.beforeUpload}
        {...props}
      >
        {fileList.length < maxLength && (
          <UploadButton>
            <Icon type="upload" /> {title}
          </UploadButton>
        )}
      </Upload>
    )
  }
}
