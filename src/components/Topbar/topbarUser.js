import React, { Component } from 'react'
import Popover from 'components/uielements/popover'
import { TopbarDropdownWrapper } from './styles'
import user_icon from 'public/images/img_user.png'
import dropdown_icon from 'public/images/img_user.png'
import { Link } from 'react-router-dom'
import { getWidth, Colors, Icons } from 'constants/constants'
import { Modal, Button } from 'antd'
import ChangePassword from 'public/images/img-doimatkhau.png'
export default class TopbarUser extends Component {
  constructor(props) {
    super(props)
    this.state = {
      visible: false,
      modalVisible: false,
      width: 0,
      height: 0
    }
  }

  componentDidMount() {
    this.updateWindowDimensions()
    window.addEventListener('resize', this.updateWindowDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions)
  }

  updateWindowDimensions = () => {
    this.setState({ width: window.innerWidth, height: window.innerHeight })
  }
  static defaultProps = {
    name: null,
    role: 'jobname',
    avatar: null
  }

  hide = () => {
    this.setState({ visible: false })
  }

  handleVisibleChange = () => {
    this.setState({ visible: !this.state.visible })
  }

  handleCancelModal = () => {
    this.setState({ modalVisible: false })
  }

  showModal = () => {
    this.setState({ modalVisible: true })
  }
  render() {
    const { name, role, avatar, logout } = this.props
    const { modalVisible, visible } = this.state

    const content = (
      <TopbarDropdownWrapper className="isoUserDropdown">
        <div style={{ display: this.state.width <= 700 ? 'block' : 'none' }}>
          <span
            style={{
              fontWeight: 'bold',
              fontSize: '15px',
              padding: '7px 15px'
            }}
          >
            {name}
          </span>
          <br />
          <span
            style={{
              fontWeight: 'bold',
              fontSize: '15px',
              padding: '7px 15px'
            }}
          >
            ({role})
          </span>
          <span>&#9;</span>
        </div>
        <div className="row isoDropdownDiv" style={{ maxWidth: '220px' }}>
          <img src={Icons.icDoiMK} height="25px" width="25px" />
          <a
            className="isoDropdownLink"
            style={{ padding: '5px 0px 0px 5px' }}
            onClick={this.showModal}
          >
            Đổi mật khẩu
          </a>
          <Modal
            visible={modalVisible}
            onCancel={this.handleCancelModal}
            footer={null}
            width={this.state.width <= 991 ? '430px' : '820px'}
            height="700px"
          >
            <div className="row">
              <div className="col-lg-6 col-md-12">
                <form className=" form-login text-center">
                  <h1>
                    <b>Đổi mật khẩu</b>
                  </h1>
                  <br />
                  <div className="form-group">
                    <input
                      className="form-control-login"
                      autoFocus
                      type="password"
                      placeholder="Nhập mật khẩu cũ"
                      style={{ letterSpacing: '5px' }}
                    />
                  </div>
                  <div className="form-group">
                    <input
                      className="form-control-login"
                      type="password"
                      placeholder="Nhập mật khẩu mới"
                      style={{ letterSpacing: '5px', color: '#727272' }}
                    />
                  </div>
                  <Button className="btn-login mt-4" onClick={this.loginUser}>
                    Đăng nhập
                  </Button>
                </form>
                <br />
              </div>
              <div className="col-lg-6 col-md-12">
                <img src={ChangePassword} height="322" width="378" />
              </div>
            </div>
          </Modal>
        </div>
        <div className="row isoDropdownDiv" style={{ maxWidth: '220px' }}>
          <img src={Icons.icDangXuat} height="25px" width="25px" />

          <a
            className="isoDropdownLink"
            style={{ padding: '5px 0px 0px 5px' }}
            onClick={logout}
          >
            Đăng xuất
          </a>
        </div>
      </TopbarDropdownWrapper>
    )
    return (
      <div className="row">
        <div style={{ height: 70 }}>
          <h6
            style={{
              height: '35px',
              lineHeight: '50px',
              fontWeight: 'bold',
              padding: '0px 0px 0px 5px',
              fontSize: '15px'
            }}
          >
            {name}
          </h6>

          <h6
            style={{
              height: 35,
              lineHeight: '15px',
              padding: '0px 20px 0px 5px',
              fontSize: '15px'
            }}
          >
            {role}
          </h6>
        </div>
        <div>
          <Popover
            content={content}
            trigger="click"
            visible={visible}
            onVisibleChange={this.handleVisibleChange}
            arrowPointAtCenter={true}
            placement="bottomLeft"
          >
            <div>
              <img
                className="mr-2"
                src={user_icon}
                style={{ width: '35px', height: '35px', borderRadius: '50%' }}
                alt="user"
              />
            </div>
          </Popover>
        </div>
      </div>
    )
  }
}
