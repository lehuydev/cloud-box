import styled from 'styled-components'
import { palette } from 'styled-theme'
import { transition, borderRadius } from 'settings/style-util'
import WithDirection from 'settings/withDirection'
import { Colors, Fonts } from '../../../constants/constants'
const OutButtonStyle = styled.a`
  > p {
    color: ${Colors.blueMain};
    font-weight: bold;
    font-size: 14px;
  
  }
  > p:hover {
  }
`

export default WithDirection(OutButtonStyle)
