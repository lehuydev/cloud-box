import React from 'react'
import { connect } from 'react-redux'
import moment from 'moment'

import { DatePicker, Select } from 'antd'
import { Link } from 'react-router-dom'

import { Button } from 'react-bootstrap'
import { actions as docActions } from 'redux/modules/document-reducer'

import {
  actions as recorddataActions,
  selectors as recorddataSelectors
} from 'redux/modules/recorddata-reducer'

const Option = Select.Option
let timeout
class AddDocument extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      typeID: '',
      typeName: '',
      description: '',
      activeDate: '',
      filter: {
        name: '',
        page_index: 1,
        page_size: 10000
      }
    }
  }
  componentWillMount() {
    this.props.setTitle('Thêm mới công văn')
  }

  componentDidMount() {
    this.getTypes()
  }

  getTypes = () => {
    const params = this.state.filter
    this.props.getRecorddatas(params)
  }

  handleSearchType = value => {
    clearTimeout(timeout)
    timeout = setTimeout(() => {
      this.setState(
        { filter: { name: value, page_index: 1, page_size: 10000 } },
        () => {
          if (this.state.filter.name === '') {
            this.setState(
              {
                filter: { name: value, page_index: 1, page_size: 10000 }
              },
              () => {
                this.getTypes()
              }
            )
          } else this.getTypes()
        }
      )
    }, 1000)
  }

  handleChangeType = (value, props) => {
    const id = parseInt(value)
    this.setState({ typeID: id, typeName: props.props.name })
  }

  onSubmit = () => {
    let { name, typeID, description, activeDate } = this.state
    const today = moment(this.state.activeDate, moment.defaultFormat).format(
      'X'
    )
    activeDate = parseInt(today)

    this.props.addDocument({
      name,
      typeID,
      description,
      activeDate
    })
    this.props.history.goBack()
  }

  render() {
    const type = this.props.recorddatas.map(d => (
      <Option key={d.id} name={d.name}>
        {d.name}
      </Option>
    ))

    return (
      <div style={{ width: '100%', height: '100%' }}>
        <form>
          <div className="form-row">
            <div className="form-group col-6">
              <label htmlFor="viName">Tên công văn</label>
              <input
                id="viName"
                type="text"
                className="form-control"
                onChange={e => {
                  this.setState({ name: e.target.value })
                }}
              />
            </div>{' '}
            <div className="form-group col-6">
              <label>Loại công văn</label>
              <Select
                showSearch
                value={this.state.typeName}
                placeholder={this.props.placeholder}
                style={this.props.style}
                defaultActiveFirstOption={false}
                showArrow={false}
                filterOption={false}
                onSearch={this.handleSearchType}
                onChange={this.handleChangeType}
                notFoundContent={null}
              >
                {type}
              </Select>
            </div>
          </div>

          <div className="form-row">
            <div className="form-group col-6">
              <label htmlFor="viName">Mô tả</label>
              <input
                id="viName"
                type="text"
                className="form-control"
                onChange={e => {
                  this.setState({ description: e.target.value })
                }}
              />
            </div>
            <div className="form-group col-6">
              <label htmlFor="staffBd">Ngày hiệu lực</label>
              <br />
              <DatePicker
                style={{ width: '100%' }}
                format={'DD-MM-YYYY'}
                placeholder="Chọn ngày hiệu lực"
                onChange={value => {
                  this.setState({
                    activeDate: moment(value).format('DD-MM-YYYY')
                  })
                }}
              />
            </div>
          </div>
        </form>
        <Button
          className="btn-small mr-3 mt-5"
          type="button"
          onClick={this.onSubmit}
        >
          Thêm
        </Button>
        <Button className="btn-theme mt-5" type="button">
          <Link to="/cong-van-chung-tu">Hủy</Link>
        </Button>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  recorddatas: recorddataSelectors.getRecorddatasList(state)
})

const mapDispatchToProps = dispatch => ({
  getRecorddatas: obj => dispatch(recorddataActions.getRecorddatas(obj)),
  addDocument: obj => dispatch(docActions.addDocument(obj))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddDocument)
