import React from 'react'
import { connect } from 'react-redux'

import client from 'client'

import { actions } from 'redux/modules/auth-reducer'

import configFactory from 'config'



class Redirect extends React.Component {
  componentDidMount() {
    this.auth()
  }

  // auth = () => {
  //   client
  //     .api(
  //       '/login', 'post'
  //     )
  //     .then(res => {
  //       if (res.status == 200) {
  //         this.props.login()

  //         localStorage.setItem('user', res.data.retData.userID)
  //         localStorage.setItem('name', res.data.retData.name)
  //       } else {
  //         alert('Opps! Something went wrong!')
  //       }

  //       const home = window.location.href.split('/')[0]
  //       window.location = home
  //     })
  // }
  auth = () => {
    console.log('redirect')

    this.props.login()

    const home = window.location.href.split('/')[0]
    window.location = home
  }
  render() {
    return <div>Redirecting...</div>
  }
}

const mapDispatchToProps = dispatch => ({
  login: user => dispatch(actions.login(user))
})

export default connect(
  null,
  mapDispatchToProps
)(Redirect)
