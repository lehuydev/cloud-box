import styled from 'styled-components'
import { palette } from 'styled-theme'
import WithDirection from 'settings/withDirection'
const SignInStyleWrapper = styled.div`
  width: 100%;
  height: 100vh;
  background-color: #f0f1f4;
  .container-login {
    box-shadow: 0 8px 16px 0 #989898;
    background-color: #ffffff;
    div.all-login-wrapper {
      div.img-login {
        img.img-full {
          width: 100%;
          padding: 30px;
        }
      }
      form.form-login {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        height: 100%;
        div.form-group {
          width: 100%;
        }
        div.text-title {
          color: #067af7;
          font-size: 48px;
          font-weight: bold;
          letter-spacing: 1.4px;
        }
        div.forgot-password {
          color: #067af7;
          font-weight: bold;
        }
      }
    }
  }
`

export default WithDirection(SignInStyleWrapper)
