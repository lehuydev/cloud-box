import React from 'react'
import { Route, withRouter, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { Icons } from 'constants/constants'

import { Dashboard } from 'components'

// define routes to use with sidebar
import routes from './routes'
import { selectors } from 'redux/modules/auth-reducer'

class PrivateRoute extends React.Component {
  state = { title: '' }

  componentWillReceiveProps(nextProps) {
    if (this.props.path !== nextProps.path) {
      this.setState({ title: '' })
    }
  }

  render() {
    const {
      user,
      path,
      title,
      history,
      location,
      routeProps,
      computedMatch,
      component: Component
    } = this.props

    let menu = JSON.parse(localStorage.getItem('menu'))
    let routes = []

    let defaultRoutesTop = {
      key: 'tong-quan', // path
      label: 'Tổng Quan',
      icon: Icons.icTongQuan,
      subItems: []
    }

    let optionalRoutes = [
      {
        key: 'thong-tin', // path
        label: 'Nhận Thông Tin',
        icon: Icons.icThongTin,
        subItems: []
      },
      {
        key: 'hoi-dap', // path
        label: 'Hỏi Đáp',
        icon: Icons.icHoiDap,
        subItems: [
          {
            title: 'FAQs',
            key: 'hoi-dap/faqs'
          },
          {
            title: 'Danh Sách Câu Hỏi',
            key: 'hoi-dap/danh-sach-cau-hoi'
          }
        ]
      },
      {
        key: 'tra-loi-tu-dong', // path
        label: 'Trả Lời Tự Động',
        icon: Icons.icTraLoiTuDong,
        subItems: []
      },
      {
        key: 'chien-dich', // path
        label: 'Chương Trình Khuyến Mãi',
        icon: Icons.icKhuyenMai,
        subItems: [
          {
            title: 'Nạp Mã',
            key: 'chien-dich/nap-ma'
          },
          {
            title: 'Danh Sách',
            key: 'chien-dich/danh-sach'
          },
          {
            title: 'Hướng Dẫn',
            key: 'chien-dich/huong-dan'
          }
        ]
      },
      {
        key: 'khach-hang-than-thiet', // path
        label: 'Khách Hàng Thân Thiết',
        icon: Icons.icKHTT,
        subItems: [
          {
            title: 'Danh Sách Khách Hàng',
            key: 'khtt/danh-sach-khach-hang'
          }
        ]
      },
      {
        key: 'pheu-khach-hang', // path
        label: 'Phễu Khách Hàng',
        icon: Icons.icPheuKH,
        subItems: [
          {
            title: 'Người Dùng',
            key: 'pheu-khach-hang/nguoi-dung'
          }
        ]
      },
      {
        key: 'piksal', // path
        label: 'Piksal',
        icon: Icons.icPheuKH,
        subItems: [
          {
            title: 'Chiến dịch',
            key: 'piksal/chien-dich'
          }
        ]
      }
    ]
    let defaultRoutesBottom = [
      {
        key: '', // path
        label: '',
        icon: '',
        subItems: []
      },
      // {
      //   key: 'phan-tich-du-lieu', // path
      //   label: 'Phân Tích Dữ Liệu',
      //   icon: Icons.icPhanTichDuLieu,
      //   subItems: []
      // },
      {
        key: 'quan-ly-nhan-su', // path
        label: 'Quản Lý Nhân Sự',
        icon: Icons.icQLNguoiDung,
        subItems: [
          {
            title: 'Nhân Viên',
            key: 'quan-ly-nhan-su/nhan-vien'
          }
        ]
      },
      {
        key: '', // path
        label: '',
        icon: '',
        subItems: []
      },
      {
        key: 'cai-dat', // path
        label: 'Cài Đặt',
        icon: Icons.icCaiDat,
        subItems: []
      }
    ]

    if (menu === null) {
    } else {
      routes.push(defaultRoutesTop)

      for (let i = 0; i < menu.length; i++) {
        routes.push(optionalRoutes[menu[i] - 1])
      }
      for (let i = 0; i < defaultRoutesBottom.length; i++) {
        routes.push(defaultRoutesBottom[i])
      }
    }

    // set active route to view on sidebar
    localStorage.setItem('currentKey', path.split(/\//)[1])
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    const signedIn = localStorage.getItem('user')
    return (
      <Dashboard title={this.state.title || title} routes={routes}>
        <Route
          {...routeProps}
          render={() =>
            signedIn ? (
              <Component
                {...computedMatch}
                history={history}
                location={location}
                setTitle={title => this.setState({ title })}
              />
            ) : (
              <Redirect
                to={{
                  pathname: '/login',
                  state: { from: location }
                }}
              />
            )
          }
        />
      </Dashboard>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    component: ownProps.component,
    user: selectors.getUser(state),
    location: ownProps.path,
    routeProps: {
      exact: ownProps.exact,
      path: ownProps.path
    }
  }
}

export default withRouter(connect(mapStateToProps)(PrivateRoute))
