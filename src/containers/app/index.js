import 'styles/global.css'
import 'antd/dist/antd.css'
import { MainStyle } from './index.style.js'

import React from 'react'
import { Route, Switch, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { IntlProvider } from 'react-intl'
import { hot } from 'react-hot-loader'
import { ThemeProvider } from 'styled-components'
import $ from 'jquery'
import themes from 'settings/themes'
import { themeConfig } from 'settings'
import jwt from 'jsonwebtoken'
import moment from 'moment'

// Auth
import Login from 'containers/login'
import Redirect from 'containers/redirect'

// Home
import HomePage from 'containers/home'
//forgotpass
// import ForgotPass from 'containers/ForgotPass'


import Users from 'containers/users/users-list'
import StaffManage from 'containers/staff/staff-list'

import Settings from 'containers/settings'

import ErrorPage404 from '@/containers/error/404'
import PrivateRoute from '@/containers/route/private-route'

import { actions, selectors } from 'redux/modules/auth-reducer'

class App extends React.Component {
  constructor(props) {
    super(props)
    this.setUser()
  }

  componentDidMount() {
    $(window).bind('popstate', function() {
      history.pushState(
        {
          id: 'homepage'
        },
        'Home | My App',
        window.location.href
      )
    })
  }
  setUser = () => {
    const token = localStorage.getItem('token')
    if (token) {
      const user = {
        userID: 123
      }

      if (!this.props.user) {
        this.props.setUser({ userID: 123 })
      }
    }
  }

  // prettier-ignore
  render() {
    return (
      <MainStyle>
        <ThemeProvider theme={themes[themeConfig.theme]}>
          <IntlProvider locale={'en'} messages={{}}>
            <Switch>
              {/* Authentication */}
              <Route exact path='/login' component={Login} />
              <PrivateRoute exact path='/' component={Login} />
              {/* <Route exact path='/redirect' component={Redirect} /> */}
              {/* <PrivateRoute path='/reports/users' component={Users} />
              {/* Home */}
              {/* <PrivateRoute exact path='/tong-quan' component={Dashbroad} /> */}
              {/* Quản lý nhân sự */}
              <PrivateRoute exact path='/pheu-khach-hang/nguoi-dung' component={Users} />
              <PrivateRoute exact path='/quan-ly-nhan-su/nhan-vien' component={StaffManage} />
              {/* Hỏi Đáp */}
              {/*<PrivateRoute exact path='hoi-dap' component={} />*/}
              {/* <PrivateRoute exact path='/hoi-dap/faqs' component={FAQ} /> */}
              {/* <PrivateRoute exact path='/hoi-dap/danh-sach-cau-hoi' component={QuestionList} /> */}

              {/* Thông Tin */}
              {/* <PrivateRoute exact path='/thong-tin' component={RecieveInfo} /> */}
              {/* Đặt Hẹn */}
              {/* <PrivateRoute exact path='/thong-tin/dat-hen' component={RecieveInfo} /> */}
              {/* Trả lời tự động */}
              {/* <PrivateRoute exact path='/tra-loi-tu-dong' component={AutoAnswer} /> */}
              {/* Khuyến mãi */}
              {/* <PrivateRoute exact path='/chien-dich/nap-ma' component={CampaignCode} /> */}
              {/* <PrivateRoute exact path='/chien-dich/danh-sach' component={CampaignList} /> */}
              {/* <PrivateRoute exact path='/chien-dich/huong-dan' component={CampaignInstruction} /> */}
              {/* <PrivateRoute exact path='/chien-dich/them-moi' component={CampaignAdd} /> */}
              {/* <PrivateRoute exact path='/chien-dich/chi-tiet' component={CampaignDetail} /> */}
              
              {/* <PrivateRoute exact path='/piksal/chien-dich' component={CampaignListPiksal} /> */}
              {/* <PrivateRoute exact path='/piksal/chien-dich/them-moi' component={CampaignAddPiksal} /> */}

              
              {/* KHTT */}
              {/* <PrivateRoute exact path='/khtt/danh-sach-khach-hang' component={KHTTList} /> */}
              {/* Phễu khách hàng */}
              {/* <PrivateRoute exact path='/pheu-khach-hang' component={FilterCustomer} /> */}
              {/* Phân tích dữ liệu */}
              {/* <PrivateRoute exact path='/phan-tich-du-lieu' component={InfoAnalyst} /> */}
              {/* Cài đặt */}
              <PrivateRoute exact path='/cai-dat' component={Settings} />
              {/* Error */}
              <Route component={ErrorPage404} />
            </Switch>
          </IntlProvider>
        </ThemeProvider>
      </MainStyle>
    )
  }
}

const mapStateToProps = state => ({
  user: selectors.getUser(state),
  redux_state: state
})

const mapDispatchToProps = dispatch => ({
  setUser: user => dispatch(actions.setUser(user)),
  getUserInfo: id => dispatch(actions.getUserInfo(id))
})

export default hot(module)(
  withRouter(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )(App)
  )
)
