import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Search from 'components/uielements/search'
import ComponentBtn from '../../components/uielements/btn'
import { Icons, Colors } from 'constants/constants'
// import infor from '../../data/requestInfor/infor.json'
import { AnalysisStyle, ContainerStyle } from './index.style.js'
import { queryPage } from 'helper'
//modal

import moment from 'moment'
import {
  Icon,
  Modal,
  Button,
  DatePicker,
  Pagination,
  Select,
  Popconfirm
} from 'antd'

const { Option } = Select
import { actions, selectors } from 'redux/modules/user-reducer'

moment.defaultFormat = 'DD/MM/YYYY'
let timeout = 0

class UserManage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      start: 0,
      end: 0,
      visible: false,
      list: [],
      notList: true,
      page: {},
      filter: {
        name: '',
        page: 1,
        limit: 20
      }
    }
  }

  componentWillMount() {
    this.props.setTitle('NGƯỜI DÙNG')
    this.getUsers()
  }
  componentDidMount() {
    setTimeout(() => {
      if (!this.getQueryPage()) {
        this.setQueryPage(1)
      }
      this.setState(
        { filter: { ...this.state.filter, page: this.getQueryPage() } },
        () => {
          this.getUsers()
        }
      )
    }, 0)
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.list) {
      if (this.props.list !== nextProps.list) {
        this.setState({ list: nextProps.list, page: nextProps.page }, () => {
          if (this.state.list !== []) {
            this.setState({ notList: false })
          }
        })
      }
    }
  }

  getUsers = () => {
    const params = this.state.filter
    this.props.getUsers(params)
  }

  //Page handler
  setQueryPage = page => {
    // return queryPage(this.props.history).set(
    //   page,
    //   '/pheu-khach-hang/nguoi-dung'
    // )
    return null
  }

  getQueryPage = () => {
    return queryPage(this.props.history).get()
  }

  onChange = page => {
    this.setState({ filter: { ...this.state.filter, page: page } }, () => {
      this.setQueryPage(page)
      this.getUsers()
    })
  }

  handleSearch = e => {
    clearTimeout(timeout)
    let value = e.target.value
    timeout = setTimeout(() => {
      this.setState({ filter: { name: value, page: 1, limit: 50 } }, () => {
        if (this.state.filter.name === '') {
          this.setState(
            {
              filter: { name: value, page: 1, limit: 20 }
            },
            () => {
              this.getUsers()
            }
          )
        } else this.getUsers()
      })
    }, 1000)
  }

  handleChangeLimit = value => {
    this.setState({ filter: { name: '', page: 1, limit: value } }, () => {
      this.getUsers()
    })
  }

  renderInfor = user => {
    const { name, address, birthdate, phone_number, email, status } = user

    return (
      <React.Fragment>
        <td>
          {/* <a href="#" onClick={() => this.showinfo(user)}> */}
          {name}
          {/* </a> */}
        </td>
        <td>{status === 0 ? 'Nam' : 'Nữ'}</td>
        <td>{birthdate}</td>
        <td>{address}</td>
        <td>{phone_number}</td>
        <td>{email}</td>
        <td>
          {/* <Link to={`/nha-cung-cap/chinh-sua?id=${id}`}>Sửa</Link> |{' '} */}
          <Link to={'/nha-cung-cap/chinh-sua?id='}>
            <img src={Icons.icCapNhat} />
          </Link>{' '}
          &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;
          <a href="#">
            <Popconfirm
              title="Bạn có chắc muốn xóa mục này ?"
              // onConfirm={this.confirm.bind(this, item)}
              okText="Xóa"
              cancelText="Hủy"
            >
              <img src={Icons.icXoa} />
            </Popconfirm>
          </a>
        </td>
      </React.Fragment>
    )
  }

  render() {
    const { visible, list, notList } = this.state
    const { isLoading, date } = this.props
    const { page } = this.state
    console.log(this.props)
    return (
      <ContainerStyle>
        <div className="row">
          <div
            className="col-auto"
            style={{
              height: '45px',
              marginLeft: 'auto',
              paddingRight: '35px'
            }}
          >
            <Search
              value={this.state.isSearch}
              onChange={this.handleSearch}
              placeholder="Tìm kiếm theo họ tên..."
              className="search-box"
            />
          </div>
        </div>

        <table
          className="table table-hover table-striped"
          id="tableTypeProduct"
          style={{ border: '1px solid #ddd' }}
        >
          <thead className="thead-light">
            <tr>
              <th scope="col">Họ tên</th>
              <th scope="col">Giới tính</th>
              <th scope="col">Sinh nhât</th>
              <th scope="col">Địa chỉ</th>
              <th scope="col">Số ĐT</th>
              <th scope="col">Email</th>
              <th scope="col">Công cụ</th>
            </tr>
          </thead>
          <tbody id="tableContent">
            {isLoading ? (
              <tr colSpan="8" className="text-center">
                <td colSpan="8">
                  <Icon type="loading" />
                </td>
              </tr>
            ) : list.length <= 0 ? (
              <tr className="text-center">
                <td colSpan="8">Không có dữ liệu</td>
              </tr>
            ) : (
              list.map(user => {
                return (
                  <tr id={user.id} key={user.id}>
                    {this.renderInfor(user)}
                  </tr>
                )
              })
            )}
          </tbody>
        </table>
        <br />
        <div style={{ display: 'inline-flex', float: 'right' }}>
          <div>
            Hiển thị:&nbsp;
            <Select
              defaultValue="20"
              style={{ marginLeft: 'auto', marginRight: '10px' }}
              onChange={this.handleChangeLimit}
            >
              <Option value={20}>20</Option>
              <Option value={50}>50</Option>
              <Option value={100}>100</Option>
            </Select>
          </div>
          <Pagination
            style={{ marginLeft: 'auto', marginRight: '10px' }}
            current={page.page}
            onChange={this.onChange}
            total={page.total}
            pageSize={this.state.filter.limit}
          />
        </div>
      </ContainerStyle>
    )
  }
}

const mapStateToProps = state => ({
  list: selectors.getUsersList(state),
  page: selectors.getPage(state),
  isLoading: selectors.getLoading(state, 'getUsers'),
  date: selectors.getStatistics(state)
  // days: selectors.exportUsers(state)
})

const mapDispatchToProps = dispatch => ({
  getUsers: obj => dispatch(actions.getUsers(obj))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserManage)
