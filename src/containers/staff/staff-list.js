import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Search from 'components/uielements/search'
import ComponentBtn from '../../components/uielements/btn'
import { Icons, Colors } from 'constants/constants'
// import infor from '../../data/requestInfor/infor.json'
import { AnalysisStyle, ContainerStyle } from './index.style.js'
import { queryPage } from 'helper'
//modal

import moment from 'moment'
import {
  Icon,
  Modal,
  Button,
  DatePicker,
  Pagination,
  Select,
  Popconfirm
} from 'antd'

const { Option } = Select
import { actions, selectors } from 'redux/modules/staff-reducer'

moment.defaultFormat = 'DD/MM/YYYY'
let timeout = 0

class StaffManage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      visible: false,
      list: [],
      page: {},
      first_name: '',
      last_name: '',
      birthdate: 0,
      gender: 0,
      address: '',
      email: '',
      phone_number: '',
      role_id: 0,
      user_type: 2,
      filter: {
        name: '',
        page: 1,
        limit: 20
      }
    }
  }

  componentWillMount() {
    this.props.setTitle('NHÂN VIÊN')
    this.getStaffs()
    this.getRoles()
  }
  componentDidMount() {
    setTimeout(() => {
      if (!this.getQueryPage()) {
        this.setQueryPage(1)
      }
      this.setState(
        { filter: { ...this.state.filter, page: this.getQueryPage() } },
        () => {
          this.getStaffs()
        }
      )
    }, 0)
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.list) {
      if (this.props.list !== nextProps.list) {
        this.setState({ list: nextProps.list, page: nextProps.page })
      }
    }
  }

  getStaffs = () => {
    const params = this.state.filter
    this.props.getStaffs(params)
  }

  getRoles = () => {
    const params = this.state.filter
    this.props.getRoles(params)
  }

  //Page handler
  setQueryPage = page => {
    return queryPage(this.props.history).set(page, '/quan-ly-nhan-su/nhan-vien')
  }

  getQueryPage = () => {
    return queryPage(this.props.history).get()
  }

  onChange = page => {
    this.setState({ filter: { ...this.state.filter, page: page } }, () => {
      this.setQueryPage(page)
      this.getStaffs()
    })
  }

  handleSearch = e => {
    clearTimeout(timeout)
    let value = e.target.value
    timeout = setTimeout(() => {
      this.setState({ filter: { name: value, page: 1, limit: 50 } }, () => {
        if (this.state.filter.name === '') {
          this.setState(
            {
              filter: { name: value, page: 1, limit: 20 }
            },
            () => {
              this.getStaffs()
            }
          )
        } else this.getStaffs()
      })
    }, 1000)
  }

  handleChangeLimit = value => {
    this.setState({ filter: { name: '', page: 1, limit: value } }, () => {
      this.getStaffs()
    })
  }

  showModal = () => {
    this.setState({
      visible: true
    })
  }

  handleOk = () => {
    this.setState({ loading: true })
    let {
      first_name,
      last_name,
      birthdate,
      gender,
      address,
      email,
      phone_number,
      user_type
    } = this.state
    let role_id = this.state.role_id
    role_id = parseInt(role_id)
    gender = parseInt(gender)
    birthdate = birthdate.format('X')
    birthdate = parseInt(birthdate)
    this.props.addStaff(
      {
        first_name,
        last_name,
        birthdate,
        gender,
        address,
        email,
        phone_number,
        user_type
      },
      role_id
    )
    setTimeout(() => {
      this.setState({ loading: false, visible: false })
      this.getStaffs()
    }, 2000)
  }

  handleCancel = e => {
    this.setState({
      visible: false,
      first_name: '',
      last_name: '',
      birthdate: 0,
      gender: 0,
      address: '',
      email: '',
      phone_number: '',
      role_id: 0
    })
  }

  renderInfor = staff => {
    let { name, address, birthdate, phone_number, email, gender } = staff
    let day = moment.unix(birthdate)
    day = day.format('DD-MM-YYYY')
    return (
      <React.Fragment>
        <td>
          {/* <a href="#" onClick={() => this.showinfo(staff)}> */}
          {name}
          {/* </a> */}
        </td>
        <td>{gender == 0 ? 'Nam' : 'Nữ'}</td>
        <td>{day}</td>
        <td>{address}</td>
        <td>{phone_number}</td>
        <td>{email}</td>
        <td>
          {/* <Link to={`/nha-cung-cap/chinh-sua?id=${id}`}>Sửa</Link> |{' '} */}
          {/* <Link to={'/nha-cung-cap/chinh-sua?id='}> */}
          <img src={Icons.icCapNhat} />
          {/* </Link>{' '} */}
          &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;
          <a href="#">
            <Popconfirm
              title="Bạn có chắc muốn xóa mục này ?"
              // onConfirm={this.confirm.bind(this, item)}
              okText="Xóa"
              cancelText="Hủy"
            >
              <img src={Icons.icXoa} />
            </Popconfirm>
          </a>
        </td>
      </React.Fragment>
    )
  }

  render() {
    const {
      visible,
      list,
      loading,
      first_name,
      last_name,
      birthdate,
      gender,
      address,
      email,
      phone_number,
      page
    } = this.state
    const { isLoading, date } = this.props
    let roleList = this.props.roleList.map(d => (
      <option value={d.id} id={d.id} name={d.name}>
        {d.name}
      </option>
    ))

    return (
      <ContainerStyle>
        <div className="row">
          <div
            className="col-auto"
            style={{ height: '45px', marginLeft: 'auto' }}
          >
            <Search
              value={this.state.isSearch}
              onChange={this.handleSearch}
              placeholder="Tìm kiếm theo họ tên..."
              className="search-box"
            />
          </div>
          <div
            style={{
              height: '45px',
              paddingRight: '20px',
              maxWidth: '320px'
            }}
          >
            <ComponentBtn
              title="Thêm mới"
              float="right"
              icon={Icons.icThemMoi}
              onClick={this.showModal}
            />
            <Modal
              // title="Xuất File"
              className="export-modal"
              visible={visible}
              onOk={this.handleOk}
              onCancel={this.handleCancel}
              width="750px"
              footer={[
                <div
                  className="row"
                  style={{ margin: 'auto', display: 'block' }}
                >
                  <Button
                    id="generalBtn"
                    className="btnGeneral"
                    type="primary"
                    loading={loading}
                    onClick={this.handleOk}
                  >
                    Thêm
                  </Button>
                  <Button
                    id="generalBtn"
                    className="btnDelete"
                    key="submit"
                    type="primary"
                    loading={loading}
                    onClick={this.handleCancel}
                  >
                    Hủy
                  </Button>
                </div>
              ]}
            >
              <form>
                <div class="form-group">
                  <div className="row">
                    <div className="col-6">
                      <label id="campaignLable" for="campaignDesc">
                        Họ
                      </label>
                      <input
                        type="text"
                        class="form-control"
                        id="campaignInput"
                        placeholder="Nhập họ"
                        value={last_name}
                        onChange={e => {
                          this.setState({ last_name: e.target.value })
                        }}
                      />
                    </div>
                    <div className="col-6">
                      <label id="campaignLable" for="campaignDesc">
                        Tên
                      </label>
                      <input
                        type="text"
                        class="form-control"
                        id="campaignInput"
                        placeholder="Nhập tên"
                        value={first_name}
                        onChange={e => {
                          this.setState({ first_name: e.target.value })
                        }}
                      />
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div className="row">
                    <div className="col-6">
                      <label id="campaignLable" for="campaignDesc">
                        Sinh nhật
                      </label>
                      <DatePicker
                        id="campaignInput"
                        format={'DD-MM-YYYY'}
                        placeholder="Chọn ngày sinh nhật"
                        onChange={value => {
                          this.setState({
                            birthdate: moment(value, 'DD-MM-YYYY')
                          })
                        }}
                      />
                    </div>
                    <div className="col-6">
                      <label id="campaignLable" for="campaignDesc">
                        Giới tính
                      </label>
                      <select
                        defaultChecked="none"
                        onChange={() => {
                          let e = document.getElementsByName('genderList')
                          this.setState({
                            gender: e[0].selectedOptions[0].value
                          })
                        }}
                        name="genderList"
                        id="campaignInput"
                        class="form-control"
                      >
                        <option value="" selected disabled hidden>
                          Chọn giới tính
                        </option>
                        <option value={0}>Nam</option>
                        <option value={1}>Nữ</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label id="campaignLable" for="campaignDesc">
                    Địa chỉ
                  </label>
                  <input
                    type="text"
                    id="campaignInput"
                    class="form-control"
                    placeholder="Nhập địa chỉ"
                    value={address}
                    onChange={e => {
                      this.setState({ address: e.target.value })
                    }}
                  />
                </div>
                <div class="form-group">
                  <label id="campaignLable" for="campaignDesc">
                    Email
                  </label>
                  <input
                    type="text"
                    id="campaignInput"
                    placeholder="Nhập email"
                    class="form-control"
                    value={email}
                    onChange={e => {
                      this.setState({ email: e.target.value })
                    }}
                  />
                </div>
                <div className="form-group">
                  <div className="row">
                    <div className="col-6">
                      <label id="campaignLable">Số điện thoại</label>
                      <input
                        type="text"
                        id="campaignInput"
                        placeholder="Nhập số điện thoại"
                        class="form-control"
                        value={phone_number}
                        onChange={e => {
                          this.setState({ phone_number: e.target.value })
                        }}
                      />
                    </div>
                    <div className="col-6">
                      <label id="campaignLable">Chọn vai trò</label>
                      <select
                        defaultChecked="none"
                        onChange={() => {
                          let e = document.getElementsByName('roleList')
                          this.setState({
                            role_id: e[0].selectedOptions[0].value
                          })
                        }}
                        name="roleList"
                        id="campaignInput"
                        class="form-control"
                      >
                        <option value="" selected disabled hidden>
                          Chọn vai trò
                        </option>
                        {roleList}
                      </select>
                    </div>
                  </div>
                </div>
              </form>
            </Modal>
          </div>
        </div>

        <table
          className="table table-hover table-striped"
          id="tableTypeProduct"
          style={{ border: '1px solid #ddd' }}
        >
          <thead className="thead-light">
            <tr>
              <th scope="col">Họ tên</th>
              <th scope="col">Giới tính</th>
              <th scope="col">Sinh nhât</th>
              <th scope="col">Địa chỉ</th>
              <th scope="col">Số ĐT</th>
              <th scope="col">Email</th>
              <th scope="col">Công cụ</th>
            </tr>
          </thead>
          <tbody id="tableContent">
            {isLoading ? (
              <tr colSpan="8" className="text-center">
                <td colSpan="8">
                  <Icon type="loading" />
                </td>
              </tr>
            ) : list.length <= 0 ? (
              <tr className="text-center">
                <td colSpan="8">Không có dữ liệu</td>
              </tr>
            ) : (
              list.map(staff => {
                return (
                  <tr id={staff.id} key={staff.id}>
                    {this.renderInfor(staff)}
                  </tr>
                )
              })
            )}
          </tbody>
        </table>
        <br />
        <div style={{ display: 'inline-flex', float: 'right' }}>
          <div>
            Hiển thị:&nbsp;
            <Select
              defaultValue="20"
              style={{ marginLeft: 'auto', marginRight: '10px' }}
              onChange={this.handleChangeLimit}
            >
              <Option value={20}>20</Option>
              <Option value={50}>50</Option>
              <Option value={100}>100</Option>
            </Select>
          </div>
          <Pagination
            style={{ marginLeft: 'auto', marginRight: '10px' }}
            current={page.page}
            onChange={this.onChange}
            total={page.total}
            pageSize={this.state.filter.limit}
          />
        </div>
      </ContainerStyle>
    )
  }
}

const mapStateToProps = state => ({
  list: selectors.getStaffsList(state),
  page: selectors.getPage(state),
  isLoading: selectors.getLoading(state, 'getStaffs'),
  roleList: selectors.getRolesList(state)
})

const mapDispatchToProps = dispatch => ({
  getStaffs: obj => dispatch(actions.getStaffs(obj)),
  getRoles: obj => dispatch(actions.getRoles(obj)),
  addStaff: (obj, role_id) => dispatch(actions.addStaff(obj, role_id))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StaffManage)
