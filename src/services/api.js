import client from 'client'

//**========================== Auth ============================ */
export const login = account => {
  return client.api('/login', 'post', account)
}

export const logout = () => {
  return client.api('/logout', 'get')
}

// export const register = info => {
//   return client.api('post', '/account/register', info)
// }

//**========================== EXAMPLE ============================ */
// Get accounts
// export const getAccounts = ({ name, page_index, page_size }) => {
//   return client.api(
//     `/account?name=${name}&page_index=${page_index}&page_size=${page_size}`,
//     'get'
//   )
// }
// // Get user info
// export const getAccountInfo = id => {
//   return client.api(`/account/${id}`, 'get')
// }
// // Delete account
// export const deleteAccount = id => {
//   return client.api(`/account/${id}`, 'delete')
// }
// // Add Account
// export const addAccount = params => {
//   return client.api('/account', 'post', params)
// }
// // Edit account
// export const editAccount = (obj, id) => {
//   return client.api(`/account/${id}`, 'put', obj)
// }
// // Change password
// export const ChangePassword = obj => {
//   return client.api('/password', 'put', obj)
// }
//**========================== Customer ============================ */

export const getCustomers = ({ name, page, limit }) => {
  return client.api(
    `/editor/users-meta?name=${name}&page=${page}&limit=${limit}`,
    'get'
  )
}

export const getCustomersStatistics = () => {
  return client.api('/editor/user-statistics', 'get')
}

export const exportCustomers = ({ from, to }) => {
  return client.api(`/editor/users-meta/export?from=${from}&to=${to}`, 'get')
}

//**========================== Staff ============================ */

export const getStaffs = ({ name, page, limit }) => {
  return client.api(
    `/editor/users?name=${name}&user_type=2&page=${page}&limit=${limit}`,
    'get'
  )
}

export const addStaff = params => {
  console.log(params)

  return client.api('/editor/user', 'post', params)
}

export const createAccount = params => {
  console.log(params)
  return client.api('/editor/account', 'post', params)
}

export const getRoles = ({ name, page, limit }) => {
  return client.api(
    `/editor/roles?name=${name}&user_type=1&page=${page}&limit=${limit}`,
    'get'
  )
}

//**========================== User ============================ */

export const getUsers = ({ name, page, limit }) => {
  return client.api(
    `/editor/users?name=${name}&user_type=1&page=${page}&limit=${limit}`,
    'get'
  )
}

//**========================== Question ============================ */

export const getQuestions = ({ name, status, page, limit }) => {
  return client.api(
    `/counselor/helpdesk?name=${name}&status=${status}&page=${page}&limit=${limit}`,
    'get'
  )
}

export const getMyQuestions = ({ page, limit }) => {
  return client.api(`/counselor/my-helpdesk?page=${page}&limit=${limit}`, 'get')
}

export const updatedQuestion = params => {
  return client.api('/counselor/helpdesk', 'put', params)
}

//**========================== Campaign ============================ */

export const getCampaigns = () => {
  return client.api('/editor/campaign', 'get')
}

export const addCampaign = obj => {
  return client.api('/editor/campaign', 'post', obj)
}

export const getCampaignInfo = id => {
  return client.api(`/editor/campaign/${id}`, 'get')
}

export const checkCode = code => {
  return client.api(`/editor/promotion-code/validate/${code}`, 'get')
}

export const redeemCode = code => {
  return client.api(`/editor/promotion-code/redeem/${code}`, 'post')
}

export const campaignsStat = () => {
  return client.api('/editor/campaigns/stat', 'get')
}

export const campaignDetailStat = id => {
  return client.api(`/editor/promotion-code/stat/${id}`, 'get')
}
//**========================== Piksal ============================ */

export const addCampaignPiksal = obj => {
  return client.api('/admin/campaign', 'post', obj)
}

export const getSupplierPiksal = () => {
  return client.api('/admin/suppliers', 'get')
}

//**========================== FAQ ============================ */

export const getFaqs = () => {
  return client.api('/editor/FQAs', 'get')
}

export const addFaq = obj => {
  return client.api('/editor/FQA', 'post', obj)
}

// export const getCampaignInfo = id => {
//   return client.api(`/editor/campaign/${id}`, 'get')
// }

// export const checkCode = code => {
//   return client.api(`/editor/promotion-code/validate/${code}`, 'get')
// }

// export const redeemCode = code => {
//   return client.api(`/editor/promotion-code/redeem/${code}`, 'post')
// }
