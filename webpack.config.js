require('dotenv').config({ silent: true })

const path = require('path')
const merge = require('webpack-merge')
const webpack = require('webpack')

// PLUGINS
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const InterpolateHtmlPlugin = require('interpolate-html-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin')
const UglifyjsWebpackPlugin = require('uglifyjs-webpack-plugin')
const OpenBrowserPlugin = require('open-browser-webpack-plugin')

const root = filePath => path.resolve(__dirname, filePath)

const port = process.env.PORT
const isProduction = process.env.NODE_ENV === 'production'

// process.env.NODE_PATH = root('src')

// BASIC CONFIGURATION

let config = {
  entry: [root('src/index.js')],
  output: {
    path: root('dist'),
    publicPath: '/',
    chunkFilename: '[name].[chunkhash:8].js',
    filename: 'public/js/[name].[hash:8].js'
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          use: 'css-loader',
          fallback: 'style-loader'
        })
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                importLoaders: 2,
                alias: {
                  public: root('public')
                }
              }
            },
            {
              loader: 'postcss-loader'
            },
            {
              loader: 'sass-loader'
            }
          ],
          fallback: 'style-loader'
        })
      },
      {
        test: /\.(png|svg|gif|jpe?g)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          'image-webpack-loader',
          {
            loader: 'file-loader',
            options: {
              name: '[name].[hash:8].[ext]',
              outputPath: 'public/images/',
              verbose: false
            }
          }
        ]
      },
      {
        test: /\.(eot|tiff|woff2|woff|ttf|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[hash].[ext]',
              outputPath: 'public/fonts/',
              verbose: false
            }
          }
        ]
      }
    ]
  },
  resolve: {
    alias: {
      '@': root('src'),
      public: root('public'),
      node_modules: root('node_modules')
    },
    extensions: ['.js', '.jsx', '.scss', '.css'],
    modules: [root('node_modules'), root('src'), root('public')]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: 'public/css/[name].[contenthash:8].css',
      allChunks: true,
      disable: !isProduction
    }),
    new FriendlyErrorsWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: root('./public/index.html')
    }),
    new InterpolateHtmlPlugin({
      TITLE: 'ReactJS',
      PUBLIC_URL: '/public',
      PROD: isProduction
    })
  ]
}

let stats = {
  modules: false,
  children: false,
  chunks: false
}

let devServer = {
  watchContentBase: true,
  port,
  stats,
  publicPath: '/',
  historyApiFallback: true,
  hot: true,
  host: '000.000.00.00'
}

// DEVELOPMENT CONFIGURATION

let development = {
  module: {
    rules: []
  },
  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new OpenBrowserPlugin({
      url:
        'http://localhost:' + port
    })
  ]
}

// PRODUCTION CONFIGURATION

let production = {
  module: {
    rules: []
  },
  plugins: [
    new CleanWebpackPlugin([root('dist/*')], { verbose: false }),
    new webpack.optimize.CommonsChunkPlugin({
      minChunks: 2,
      children: true
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    new UglifyjsWebpackPlugin({
      sourceMap: true
    })
  ]
}

if (process.env.NODE_ENV !== 'production') {
  // DEVELOPMENT MODE
  config = merge(merge(config, { devServer }), development)
} else {
  // PRODUCTION MODE
  config = merge(merge(config, { devServer }), production)
}

module.exports = config
